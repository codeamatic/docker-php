PHP
================

 Repository used for creating a PHP based Apache.  Similar to the official [PHP] (https://hub.docker.com/_/php/) repository, but adds iconv, mcrypt and mysql support.
 
 ```
 Command Line:
 
 docker run -d -p 80 codeamatic/php:[[TAG]]
 
 Docker-compose
 
 php:
   image: codeamatic/php
   ports:
     - "80"
   links:
     - DBNAME (if applicable)
 ```